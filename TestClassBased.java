// author: René Haberland, 19th July 2016
//
// IMPORTANT NOTICE:
//  purely experimental, use on own risk only, no guarantee whatsoever
//  may not be used/modified without the author's knowledge

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

public class TestClassBased {

	public static void main(String[] args) throws Exception {
		ANTLRInputStream input = new ANTLRInputStream(System.in);
		ClassBasedLexer lexer = new ClassBasedLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		ClassBasedParser parser = new ClassBasedParser(tokens);

		/*ParseTree tree =*/ parser.prog();
		//System.out.println(tree.toStringTree(parser));
	}

}
