#!/bin/sh

# author: René Haberland, 19th July 2016

# IMPORTANT NOTICE:
#  purely experimental, use on own risk only, no guarantee whatsoever
#  may not be used/modified without the author's knowledge

TMPFILE=_tmp
TMPFILE2=_tmp2
TMPFILE3=_tmp3

# check mandatory files
if [ ! -f TestClassBased.class -o ! -f IRProlog.class -o ! -f astbuild.pl ]
then
 echo "Failure: Make sure the compiler finds: TestClassBased.class, IRProlog.class, astbuild.pl !"
 exit 3
fi

# clean up temporary files from previous runs
rm -f dotgraph.pl $TMPFILE $TMPFILE2 $TMPFILE2 $TMPFILE3

if [ $# -ne 2 ] || [ ! -f $1 ]
then
 echo Compile a given C/C++ program into intermediate PROLOG term
 echo "Usage: sh compile.sh sample.cpp out.pl"
 exit 1
fi

# C/CPP file => IRProlog term
java TestClassBased < $1 > $TMPFILE
echo "java TestClassBased < $1 > $TMPFILE"

# check IRProlog term is sound
echo "java IRProlog < $TMPFILE > $TMPFILE2 2> $TMPFILE3"
java IRProlog < $TMPFILE > $TMPFILE2 2> $TMPFILE3
if [ $? -ne 0 ]
then
 echo "FAILURE: Could not compile IRProlog!"
 NUM_FAILS=`expr $NUM_FAILS + 1`
fi
CHAR_CNT=`wc -m $TMPFILE2 | awk '{ print $1 }'`
if [ $CHAR_CNT -ne 0 ]
then
  echo "ERROR: $1 Invalid IR ! "
  echo
  echo "Here is the error:"
  echo ">>>>>>>>>>>>>>>>>>"
  cat $TMPFILE2
  echo "<<<<<<<<<<<<<<<<<<"
  exit 5
fi
TERM=`cat $TMPFILE`

# save Prolog predicate for further process; outputs file $2
echo "input1($TERM)." > $2
echo " SAVED to $2 !"

# generate DOT representation (for debug); outputs file xdot.dot
echo "cat $2 astbuild.pl > dotgraph.pl"
cat $2 astbuild.pl > dotgraph.pl
GPROLOG_GOAL="consult(dotgraph), input1(_X), dots(1,_,_X,_Res), visit(_Res), halt"   # REMINDER: do not forget to emit result to console!
GPROLOG_GOAL=`echo $GPROLOG_GOAL | tr -d ' '`
echo "gprolog --query-goal $GPROLOG_GOAL > $TMPFILE"
gprolog --query-goal $GPROLOG_GOAL > $TMPFILE
if [ $? -ne 0 ]
then
  echo "Failed call to Prolog with script $TMPFILE2"
  exit 6
fi
echo "tail -n +7 $TMPFILE > xdot.dot"
tail -n +7 $TMPFILE > xdot.dot
#echo "xdot xdot.dot"
#xdot xdot.dot

exit 0

