#!/bin/sh

# author: René Haberland, 19th July 2016
#
# IMPORTANT NOTICE:
#  purely experimental, use on own risk only, no guarantee whatsoever
#  may not be used/modified without the author's knowledge

TMPFILE=tmp_
TMPFILE2=tmp2_
TMPFILE3=tmp3_

NUM_PROCESSED=0
NUM_FAILS=0

FILES=`find tests -name *.cpp`
#FILES="tests/1.cpp"

## currently only checks AST against comparison files
regression_test ()
{
  if [ $# -ne 1 ]
  then
   echo "You need to pass the CPP file that is going to be diffed against!"
   exit 1
  fi
  java TestClassBased < $1 > $TMPFILE
  FILENAME=`echo $1 | sed 's/\.cpp//'`
  F_AST=$FILENAME"_ast.txt"
  diff -q $TMPFILE $F_AST > $TMPFILE2 2> $TMPFILE3
  if [ $? -ne 0 ]
  then
   echo "ERROR: $F_AST did not match (see $TMPFILE)!"
   NUM_FAILS=`expr $NUM_FAILS + 1`
   #exit 5
  else
   echo "PASSED: $F_AST."
  fi
  java IRProlog < $F_AST > $TMPFILE2 2> $TMPFILE3
  if [ $? -ne 0 ]
  then
   echo "FAILURE: Could not compile IRProlog!"
   NUM_FAILS=`expr $NUM_FAILS + 1`
  else
   echo " SOUND IR FOR $1!"
  fi
  CHAR_CNT=`wc -m $TMPFILE2 | awk '{ print $1 }'`
  if [ $CHAR_CNT -ne 0 ]
  then
    echo "ERROR: $1 generated corrupt intermediate representation as output! "
    NUM_FAILS=`expr $NUM_FAILS + 1`
  fi
  # emit and test generated DOT dump
  TERM=`cat $F_AST`
  echo "input1($TERM)." > $TMPFILE2
  # generate DOT representation (for debug); outputs file xdot.dot
  cat $TMPFILE2 astbuild.pl > $TMPFILE3.pl
  GPROLOG_GOAL="consult($TMPFILE3), input1(_X), dots(1,_,_X,_Res), visit(_Res), halt"
  GPROLOG_GOAL=`echo $GPROLOG_GOAL | tr -d ' '`
  gprolog --query-goal $GPROLOG_GOAL > $TMPFILE
  if [ $? -ne 0 ]
  then
    echo "Failed call to Prolog with script $TMPFILE2"
    exit 6
  fi
  tail -n +7 $TMPFILE > $TMPFILE2
  diff -q $TMPFILE2 $FILENAME.dot > $TMPFILE 2> $TMPFILE3
  if [ $? -ne 0 ]
  then
   echo "ERROR: $1 generated invalid DOT (temporary dump backed up in $TMPFILE)! "
   NUM_FAILS=`expr $NUM_FAILS + 1`
   exit 77
  fi
  NUM_PROCESSED=`expr $NUM_PROCESSED + 1`
}

cleanup_temporary()
{
  if [ -f $TMPFILE ]; then
   rm $TMPFILE
  fi
  if [ -f $TMPFILE2 ]; then
   rm $TMPFILE2
  fi
  if [ -f $TMPFILE3 ]; then
   rm $TMPFILE3
  fi
}

summary()
{
  echo
  if [ $NUM_FAILS -ne 0 ]
  then
    echo "Tests failed: $NUM_FAILS out of $NUM_PROCESSED"
    exit 1
  else
   echo "All $NUM_PROCESSED tests passed successful!"
   exit 0
  fi
}


main()
{
  for F in $FILES; do regression_test $F; done
  cleanup_temporary
  summary
}

main
