% author: René Haberland, 19th July 2016

% IMPORTANT NOTICE:
%  purely experimental, use on own risk only, no guarantee whatsoever
%  may not be used/modified without the author's knowledge

%% BUG1: CRectangle not allowed to be upper-case!!
%% BUG2: implement classes from Grammar!  !!! CONTINUE HERE !!!
%% BUG3: write regression tests for IRProlog -> DOT for all tests!

% increments the Old number by one
fresh_Idx(Old,New):-New is Old+1.

% dump to standard out ready to display DOT-digraph notation
visit2(label(Idx,Label)):-
 write(' '),
 write(Idx),
 write(' [label="'),
 write(Label),
 write('"];\n').

visit2(label(Idx,Label,_)):-
 write(' '),
 write(Idx),
 write(' [label="'),
 write(Label),
 write('",shape=box];\n').

visit2((FROM,TO)):-
 write(' '),
 write(FROM),
 write(' -> '),
 write(TO),
 write(';\n').

visit2([]).
visit2([H|T]):-!,visit2(H),visit2(T).

visit(T):-
 write('digraph graphname{\n'),
 visit2(T),
 write('}').



% -----------------------
dot_expr(Idx,LastIdx,Int,Res):-
 integer(Int),!, fresh_Idx(Idx,LastIdx),
Res=[
 label(Idx,int),
 label(LastIdx,Int),
 (Idx,LastIdx)
].

dot_expr(Idx,LastIdx,Term,Res):-
 Term=..[Op|[Expr1,Expr2]], member(Op,[add,sub,mul]), !,
 fresh_Idx(Idx,NewIdx1), 
 dot_expr(NewIdx1,NewIdx2,Expr1,ResExpr1),
 fresh_Idx(NewIdx2,NewIdx3),
 dot_expr(NewIdx3,LastIdx,Expr2,ResExpr2),
ResADD=[
 label(Idx,Op),
 (Idx,NewIdx1),
 (Idx,NewIdx3)
],
 append(ResADD,ResExpr1,Res2),
 append(Res2,ResExpr2,Res).

dot_expr(Idx,LastIdx,funcall(ID),Res):-
 !,dot(Idx,LastIdx,funcall(ID),Res).

dot_expr(Idx,LastIdx,funcall(ID,ACT_PARAMS),Res):-
 !,dot(Idx,LastIdx,funcall(ID,ACT_PARAMS),Res).

dot_expr(Idx,LastIdx,mem(LOC),Res):-
 !, fresh_Idx(Idx,NewIdx1),
 dot_loc(NewIdx1,LastIdx,LOC,ResLoc),
ResMemExpr=[
 label(Idx,mem),
 (Idx,NewIdx1)
],
 append(ResMemExpr,ResLoc,Res).

dot_expr(Idx,LastIdx,LOC,Res):-
%write('\ndot_expr: '), write(dot_loc(Idx,LastIdx,LOC,Res)),
 !,dot_loc(Idx,LastIdx,LOC,Res).

% [AST,..] -> []
dots_expr(Idx,Idx,[],[]).
dots_expr(Idx,LastIdx,[H],Res):-
 !,dot_expr(Idx,LastIdx,H,Res).
dots_expr(Idx,LastIdx,[H|T],Res):-
 dot_expr(Idx,NewIdx,H,NewRes),T=[_|_],
 fresh_Idx(NewIdx,NewIdx2),
 dots_expr(NewIdx2,LastIdx,T,NewRes2),!,
 append(NewRes,NewRes2,Res).
dots_expr(_,_,X,_):-
 nonvar(X),compound(X),!,fail.


%TEST!
dot_loc(Idx,LastIdx,offset(LOC_1),Res):-
 !, fresh_Idx(Idx,NewIdx1),
 dot_loc_1(NewIdx1, LastIdx, LOC_1, ResLoc1), 
ResLoc=[
 label(Idx,offset),
 (Idx,NewIdx1)
],
 append(ResLoc,ResLoc1,Res).

dot_loc(Idx,LastIdx,offset(LOC_1,OFFSET),Res):-
 !, fresh_Idx(Idx,NewIdx1),
 dot_loc_1(NewIdx1, NewIdx2, LOC_1, ResLoc1),
 fresh_Idx(NewIdx2,NewIdx3),
 dot_offset(NewIdx3,LastIdx,OFFSET,ResOffset), 
ResLoc=[
 label(Idx,offset),
 (Idx,NewIdx1),
 (Idx,NewIdx3)
],
 append(ResLoc,ResLoc1,Res2),
 append(Res2,ResOffset,Res).

dot_loc(Idx,LastIdx,LOC_1,Res):-
 !, dot_loc_1(Idx, LastIdx, LOC_1, Res).

%loc_1: OA LPAREN ID COMMA ID RPAREN ;
dot_loc_1(Idx,LastIdx,oa(ID1,ID2),Res):-
 !, atom(ID1),atom(ID2),
 fresh_Idx(Idx,NewIdx1), fresh_Idx(NewIdx1,NewIdx2), fresh_Idx(NewIdx2,NewIdx3), fresh_Idx(NewIdx3,LastIdx),
Res=[
  label(Idx,oa),
  label(NewIdx1,id),
  label(NewIdx2,ID1),
  label(NewIdx3,id),
  label(LastIdx,ID2),
  (Idx,NewIdx1),
  (Idx,NewIdx3),
  (NewIdx1,NewIdx2),
  (NewIdx3,LastIdx)
].
 
%loc_1: ID
dot_loc_1(Idx,LastIdx,ID,Res):-
 !, atom(ID), fresh_Idx(Idx,LastIdx),
Res=[
 label(Idx,id),
 label(LastIdx,ID),
 (Idx,LastIdx)
].

%offset: INT
dot_offset(Idx,LastIdx,INT,Res):-
 integer(INT),!,INT>0, fresh_Idx(Idx,LastIdx),
Res=[
 label(Idx,int),
 label(LastIdx,INT),
 (Idx,LastIdx)
].

%offset: MINUS LPAREN 0 COMMA INT RPAREN ;
dot_offset(Idx,LastIdx,minus(0,INT),Res):-
 !, integer(INT),INT>0, fresh_Idx(Idx,NewIdx1), fresh_Idx(NewIdx1,NewIdx2), fresh_Idx(NewIdx2,LastIdx),
Res=[
 label(Idx,minus),
 label(NewIdx1,0),
 label(NewIdx2,int),
 label(LastIdx,INT),
 (Idx,NewIdx1),
 (Idx,NewIdx2),
 (NewIdx2,LastIdx)
].

% cond: ( AND | OR | rel ) LPAREN expr COMMA expr RPAREN, where rel: LT | LE | EQ | NE | GT | GE ; 
dot_cond(Idx,LastIdx,Term,Res):-
 Term=..[Rel|[Expr1,Expr2]], member(Rel,[and,or,lt,le,eq,ne,gt,ge]), !,
 fresh_Idx(Idx,NewIdx1), fresh_Idx(NewIdx1,NewIdx2),
 dot_expr(NewIdx2,NewIdx3,Expr1,Res_Expr1), Res_Expr1=[H1|_],
 fresh_Idx(NewIdx3,NewIdx4), fresh_Idx(NewIdx4,NewIdx5),
 dot_expr(NewIdx5,LastIdx,Expr2,Res_Expr2), Res_Expr2=[H2|_],
ResCond=[
 label(Idx,Rel),
 (Idx,NewIdx1),
 (Idx,NewIdx4),
 label(NewIdx1,expression),   %% TODO: remove redundant 'expression' labels!
 (NewIdx1,NewIdx2),
 label(NewIdx4,expression),
 (NewIdx4,NewIdx5)
],
 append(ResCond,Res_Expr1,Res2),
 append(Res_Expr2,[H1,H2],Res3),
 append(Res2,Res3,Res).

% cond: NE LPAREN expr RPAREN
dot_cond(Idx,LastIdx,ne(Expr),Res):-
 !, fresh_Idx(Idx,NewIdx1),
 dot_expr(NewIdx1,LastIdx,Expr,ResExpr), ResExpr=[H1|_],
ResCond=[
 label(Idx,ne),
 (Idx,NewIdx1) 
],
 append(ResCond,ResExpr,Res2),
 append(Res2,[H1],Res).

% cond: TRUE | FALSE ;
dot_cond(Idx,Idx,BOOL,Res):-
 member(BOOL, [true,false]), !,
Res=[label(Idx,BOOL)].

% ------------------------


% traverse AST and collect DOT edges to be generated
% AST->[DOT]

dot(Idx,LastIdx,param(PName,Type),Res):-
 !,fresh_Idx(Idx,NewIdx1),
 fresh_Idx(NewIdx1,LastIdx),
 %
 Res=[
label(Idx,param),
label(NewIdx1,PName),
label(LastIdx,Type),
(Idx,NewIdx1),
(Idx,LastIdx)
 ].

dot(Idx,Idx,Numb,Res):-
 integer(Numb),!,
 Res=[
label(Idx,Numb) % TODO: add 'shape=box' for leafs (attribute)!
].

dot(Idx,Idx,return,Res):-
 !, Res=[label(Idx,return)].

dot(Idx,LastIdx,return(Expr),Res):-
 !, fresh_Idx(Idx,NewIdx1),
 dot_expr(NewIdx1,LastIdx,Expr,ResExpr),
ResReturn=[
 label(Idx,return),
 (Idx,NewIdx1)
],
 append(ResReturn,ResExpr,Res).

%alloc(oa(id,field))
dot(Idx,LastIdx,alloc(oa(ID,FIELD)),Res):-
 !, fresh_Idx(Idx,NewIdx1), fresh_Idx(NewIdx1,NewIdx2), fresh_Idx(NewIdx2,LastIdx),
 Res=[
label(Idx,alloc),
label(NewIdx1,oa),
label(NewIdx2,ID),
label(LastIdx,FIELD),
(Idx,NewIdx1),
(NewIdx1,NewIdx2),
(NewIdx1,LastIdx)
].

%alloc(id)
dot(Idx,LastIdx,alloc(ID),Res):-
 !, fresh_Idx(Idx,LastIdx),
 Res=[
label(Idx,alloc),
label(LastIdx,ID),
(Idx,LastIdx)
].

dot(Idx,LastIdx,delete(oa(ID,FIELD)),Res):-
 !, fresh_Idx(Idx,NewIdx1), fresh_Idx(NewIdx1,NewIdx2), fresh_Idx(NewIdx2,LastIdx),
 Res=[
label(Idx,delete),
label(NewIdx1,oa),
label(NewIdx2,ID),
label(LastIdx,FIELD),
(Idx,NewIdx1),
(NewIdx1,NewIdx2),
(NewIdx1,LastIdx)
].

dot(Idx,LastIdx,delete(ID),Res):-
 !, fresh_Idx(Idx,LastIdx),
 Res=[
label(Idx,delete),
label(LastIdx,ID),
(Idx,LastIdx)
].

% ITE LPAREN cond COMMA block RPAREN -TEST!!
dot(Idx,LastIdx,ite(COND,BLOCK),Res):-
 !, fresh_Idx(Idx,NewIdx1),
 dot_cond(NewIdx1,NewIdx2,COND,ResCond),
 fresh_Idx(NewIdx2,NewIdx3),
 fresh_Idx(NewIdx3,NewIdx4),
 %dots(NewIdx4,LastIdx,BLOCK,ResBlocks  % top_expr ??
 top_dots(NewIdx4,LastIdx,BLOCK,ResBlock,Tops),
 linkEdges(NewIdx3,Tops,ResBlock2),  %% AAAAAA
ResITE=[    
 label(Idx,ite),
 label(NewIdx1,cond),
 label(NewIdx3,block),
 (Idx,NewIdx1),
 (Idx,NewIdx3)
],
 append(ResITE,ResCond,Res2),
 append(ResBlock,ResBlock2,Res3),
 append(Res2,Res3,Res).

% ITE LPAREN cond COMMA block COMMA block RPAREN  -TEST!!
dot(Idx,LastIdx,ite(COND,BLOCK_IF,BLOCK_ELSE),Res):-
 !, fresh_Idx(Idx,NewIdx1), fresh_Idx(NewIdx1,NewIdx2),
 dot_cond(NewIdx2,NewIdx3,COND,ResCond),
 %
 fresh_Idx(NewIdx3,NewIdx4), fresh_Idx(NewIdx4,NewIdx5),
 top_dots(NewIdx5,NewIdx6,BLOCK_IF,ResBlock_IF,Tops_IF),
 linkEdges(NewIdx4,Tops_IF,ResBlock_IF2),  %% to append: ResBlock_IF, ResBlock_IF2
 %
 fresh_Idx(NewIdx6,NewIdx7), fresh_Idx(NewIdx7,NewIdx8),
 top_dots(NewIdx8,LastIdx,BLOCK_ELSE,ResBlock_ELSE,Tops_ELSE),
 linkEdges(NewIdx7,Tops_ELSE,ResBlock_ELSE2),  %% to append: ResBlock_ELSE, ResBlock_ELSE2
ResITE=[
 label(Idx,ite),
 label(NewIdx1,cond),
 (Idx,NewIdx1),
 (NewIdx1,NewIdx2),
 %
 label(NewIdx4,if_block),
 %
 label(NewIdx7,else_block)
],
 append(ResCond,ResITE,Res1),
 append(ResBlock_IF,ResBlock_IF2,Res2),
 append(ResBlock_ELSE,ResBlock_ELSE2,Res3),
 append(Res1,Res2,Res4),
 append(Res4,Res3,Res).


% WHILE LPAREN cond COMMA block RPAREN ;  -TEST!!
dot(Idx,LastIdx,while(COND,BLOCK),Res):-
 !, fresh_Idx(Idx,NewIdx1), fresh_Idx(NewIdx1,NewIdx2),
 dot_cond(NewIdx2,NewIdx3,COND,ResCond), %% BUGGY!
write('\nCOND: '), write(COND),
write('\nResCond'), write(ResCond),
 fresh_Idx(NewIdx3,NewIdx4), fresh_Idx(NewIdx4,NewIdx5),
 %
%write('\n in while: '), write(top_dots(NewIdx5,LastIdx,BLOCK,ResBlock,Tops)),
 top_dots(NewIdx5,LastIdx,BLOCK,ResBlock,Tops),
 linkEdges(NewIdx4,Tops,ResBlock2),
%write('\nTops:'), write(Tops),
ResITE=[    
 label(Idx,while),
 label(NewIdx1,cond),
 label(NewIdx4,block),
 (Idx,NewIdx1),
 (Idx,NewIdx4),
 (NewIdx1,NewIdx2)
],
 append(ResCond,ResITE,Res2), %append([],ResITE,Res2),
 append(ResBlock,ResBlock2,Res3), % [],Res3),
 append(Res2,Res3,Res).


% funcall: FUNCALL LPAREN ID RPAREN
dot(Idx,LastIdx,funcall(ID),Res):-
 !, fresh_Idx(Idx,LastIdx),
Res=[
 label(Idx,funcall),
 label(LastIdx,ID),
 (Idx,LastIdx)
].


%    X = [  function(foo,int,[],[funcall(foo,[a32,b,add(3,sub(1,5)),f])])  ]

%%TODO: !!!
%| FUNCALL LPAREN ID COMMA act_params RPAREN,    ACT_PARAMS is an expression list
% example: funcall(foo,[a32,b,add(3,sub(1,5)),f])
dot(Idx,LastIdx,funcall(ID,ACT_PARAMS),Res):-
 !, fresh_Idx(Idx,NewIdx1), fresh_Idx(NewIdx1,NewIdx2), fresh_Idx(NewIdx2,NewIdx3),
 top_dots_expr(NewIdx3,LastIdx,ACT_PARAMS,ResParams,Tops),
 linkEdges(NewIdx2,Tops,ResParams2),
ResFuncall=[
 label(Idx,funcall),
 label(NewIdx1,ID),
 label(NewIdx2,act_params),
 (Idx,NewIdx1),
 (Idx,NewIdx2)
],
 append(ResFuncall,ResParams,Res2),
 append(Res2,ResParams2,Res).

dot(Idx,LastIdx,variable(VarName,Type),Res):-
 !,fresh_Idx(Idx,NewIdx1),
 fresh_Idx(NewIdx1,LastIdx),
Res=[
 label(Idx,variable),
 label(NewIdx1,VarName),
 label(LastIdx,Type),
 (Idx,NewIdx1),
 (Idx,LastIdx)
].

dot(Idx,LastIdx,variable(VarName,Type,Expression),Res):-
 !,fresh_Idx(Idx,NewIdx1),
 fresh_Idx(NewIdx1,NewIdx2),
 fresh_Idx(NewIdx2,NewIdx3),
 fresh_Idx(NewIdx3,NewIdx4),
 dot_expr(NewIdx4,LastIdx,Expression,ResExpr),
ResVar=[
 label(Idx,variable),
 label(NewIdx1,VarName),
 label(NewIdx2,Type),
 label(NewIdx3,expression),
 (Idx,NewIdx1),
 (Idx,NewIdx2),
 (Idx,NewIdx3),
 (NewIdx3,NewIdx4)
],
 append(ResVar,ResExpr,Res).

dot(Idx,LastIdx,assign(VarName,Expression),Res):-
 !,fresh_Idx(Idx,NewIdx1),
 fresh_Idx(NewIdx1,NewIdx2),
 fresh_Idx(NewIdx2,NewIdx3),
 dot_expr(NewIdx3,LastIdx,Expression,ResExpr),
 ResAsn=[
label(Idx,assign),
label(NewIdx1,VarName),
label(NewIdx2,expression),
(Idx,NewIdx1),
(Idx,NewIdx2),
(NewIdx2,NewIdx3)
 ],
 append(ResAsn,ResExpr,Res).


%input_class1( class(cRectangle,[variable(x,int),variable(y,int), function(set_values,void,[param(a,int),param(b,int)],[]), function(area,int,[],[funcall(set_values,[15,4]),return(sub(mul(y,y),x)),return(mul(x,y)),assign(xyz,51)]), variable(a,int)])  ).


%class_def:
% CLASS LPAREN ID COMMA LSB fms RSB RPAREN ;

dot(Idx,LastIdx,class(ID,FMS),Res):-
 !,atom(ID), fresh_Idx(Idx,NewIdx1), fresh_Idx(NewIdx1,NewIdx2), fresh_Idx(NewIdx2,NewIdx3),
 %dots(NewIdx3,LastIdx,FMS,ResFMS),
 top_dots_fms(NewIdx3,LastIdx,FMS,ResFMS,Tops),
 linkEdges(NewIdx2,Tops,ResEdges),
ResClass=[
 label(Idx,class),
 label(NewIdx1,ID),
 label(NewIdx2,fields_methods),
 (Idx,NewIdx1),
 (Idx,NewIdx2)
],
 append(ResClass,ResEdges,Res2),
 append(Res2,ResFMS,Res).

%fms:
% fm ( COMMA fm )* | ;

%fm: func | local_vars ;


% function
dot(Idx,LastIdx,function(FName,RetType,Params,Stmts),Res):-
write('\nAAAAA'),
 fresh_Idx(Idx,NewIdx1), % FName
 fresh_Idx(NewIdx1,NewIdx2), % RetType
 fresh_Idx(NewIdx2,NewIdx3), % Params
 fresh_Idx(NewIdx3,NewIdx4),
write('\nBBBBB'),
 dots(NewIdx4,LastNewIdx4,Params,ResParams),    % possible bug?!: process each param individually, then concat results and link under 'params', see 'dots2'
write('\nCCCCC'),
 fresh_Idx(LastNewIdx4,NewIdx5), % Stmts
 fresh_Idx(NewIdx5,NewIdx6),
write('\n call: '), write(dots2(NewIdx6,LastIdx,Stmts,ResStmts,T_Idx)),
 dots2(NewIdx6,LastIdx,Stmts,ResStmts,T_Idx),
write('\nDDDDD'),
write('\nResStmts: '), write(ResStmts),
write('\nT_Idx: '), write(T_Idx),
 ResFunc = [
 % label definitions
label(Idx,function),
label(NewIdx1,FName),
label(NewIdx2,RetType),
label(NewIdx3,params),
label(NewIdx5,stmts),
 % edges
(Idx,NewIdx1),
(Idx,NewIdx2),
(Idx,NewIdx3),
%(Idx,NewIdx4),
(Idx,NewIdx5)
 ],
 % add transitions from 'params' to its children (NewIdx3,.) pairs
 !,
 linkParams(NewIdx3,ResParams,Params2Param),
 linkEdges(NewIdx5,ResStmts,Stmts2Stmt),
 unlinkTransitiveEdges(T_Idx,Stmts2Stmt,Stmts3Stmt),
 append(ResFunc,Params2Param,Res1),
 append(Res1,ResParams,Res2),
 append(Res2,ResStmts,Res3),
 append(Res3,Stmts3Stmt,Res).

dot(Idx,LIdx,Node,Res):-!,write('\nNo alternative found for sub-goal: '),write(dot(Idx,LIdx,Node,Res)),fail.

% same as dots_expr, but also keep track of the list of top IR-elements of Res
top_dots_expr(Idx,Idx,[],[],[]):-!.
top_dots_expr(Idx,LastIdx,[Expr|T],Res,Tops):-
 dot_expr(Idx,NewIdx1,Expr,Res1), Res1=[H1|_], Tops1=[H1],
 fresh_Idx(NewIdx1,NewIdx2),
 top_dots_expr(NewIdx2,LastIdx,T,Res2,Tops2),
 append(Res1,Res2,Res),
 append(Tops1,Tops2,Tops).

top_dots_fms(Idx,Idx,[],[],[]):-!.
top_dots_fms(Idx,LastIdx,[FM|T],Res,Tops):-
 dot(Idx,NewIdx1,FM,Res1), Res1=[H1|_], Tops1=[H1],
 fresh_Idx(NewIdx1,NewIdx2),
 top_dots_fms(NewIdx2,LastIdx,T,Res2,Tops2),
 append(Res1,Res2,Res),
 append(Tops1,Tops2,Tops).

top_dots(Idx,Idx,[],[],[]):-!.
top_dots(Idx,LastIdx,[FM|T],Res,Tops):-
 dot(Idx,NewIdx1,FM,Res1), Res1=[H1|_], Tops1=[H1],
 fresh_Idx(NewIdx1,NewIdx2),
 top_dots(NewIdx2,LastIdx,T,Res2,Tops2),
 append(Res1,Res2,Res),
 append(Tops1,Tops2,Tops).



% filters from: [label(5,param),label(6,a),label(7,int),(5,6),(5,7),label(8,param),label(9,b),label(10,int),(8,9),(8,10)] -> [(4,5),(4,8)]
 % TODO: refactor via filter as 'linkEdges' !
linkParams(_,[],[]):-!.
linkParams(TopIdx,[TermNode|T],Res):-
 TermNode=..[label|[Idx,Node]],
 member(Node,[param]),!,
 linkParams(TopIdx,T,Res2),
 append([(TopIdx,Idx)],Res2,Res).
linkParams(TopIdx,[_|T],Res):-
 linkParams(TopIdx,T,Res).

%% List = [label(12,assign),label(13,a),label(14,expression),(12,13),(12,14),(14,15),label(15,7),label(16,assign),label(17,c),label(18,expression),(16,17),(16,18),(18,19),label(19,15)]   returns [12,16]   is finally constructed to:  [(11,12),(11,16)]

% filter known complex nodes before additional edges are linked
%% TODO: rewrite with 'filter' and 'foldr/foldl'
linkEdges(_,[],[]):-!.
linkEdges(TopIdx,[TermNode|T],Res):-
 TermNode=..[label|[Idx,Node]],
 member(Node,[assign,while,ite,variable,alloc,delete,funcall,id,int,add,sub,mul,return,function]),!,
 linkEdges(TopIdx,T,Res2),
 append([(TopIdx,Idx)],Res2,Res).
linkEdges(TopIdx,[_|T],Res):-
 linkEdges(TopIdx,T,Res).

% filters from Xs::=[(x1,y1),(x2,y2),...] all pairs for yj in T_Idx
% Example: T_Idx: [32,37], Xs: [(31,32),(31,35),(31,37)], Res: [(31,32),(31,37)]
%%% unlinkTransitiveEdges(T_Idx,Xs,Res) /3
% TODO: refactor using 'filter'!
unlinkTransitiveEdges(_,[],[]):-!.
unlinkTransitiveEdges(Tops,[H|T],[H|T2]):-
 H=(_,TO),
 member(TO,Tops),!,
 unlinkTransitiveEdges(Tops,T,T2).
unlinkTransitiveEdges(Tops,[_|T],[T2]):-
 !,unlinkTransitiveEdges(Tops,T,T2).


% [AST,..] -> []
dots(Idx,Idx,[],[]).
dots(Idx,LastIdx,[H],Res):-
 !,dot(Idx,LastIdx,H,Res).
dots(Idx,LastIdx,[H|T],Res):-
 dot(Idx,NewIdx,H,NewRes),T=[_|_],
 fresh_Idx(NewIdx,NewIdx2),
 dots(NewIdx2,LastIdx,T,NewRes2),!,
 append(NewRes,NewRes2,Res).
dots(_,_,X,_):-
 nonvar(X),compound(X),!,fail.
dots(_,_,_,_):-!,fail.

%% ----- same as dots but with list of processed top node numbers ----------------
dots2(Idx,Idx,[],[],[]).
dots2(Idx,LastIdx,[H],Res,[Idx]):-
 !,
write('\nCallA: '), write(dot(Idx,LastIdx,H,Res)),
 dot(Idx,LastIdx,H,Res).
dots2(Idx,LastIdx,[H|T],Res,[Idx|T_Idx]):-
write('\nCallB: '), write(dot(Idx,NewIdx,H,NewRes)),
 dot(Idx,NewIdx,H,NewRes),T=[_|_],
 fresh_Idx(NewIdx,NewIdx2),
 dots2(NewIdx2,LastIdx,T,NewRes2,T_Idx),!,
 append(NewRes,NewRes2,Res).
dots2(_,_,X,_,_):-
 nonvar(X),compound(X),!,fail.
dots2(_,_,_,_,_):-!,fail.
%% ---------------------

