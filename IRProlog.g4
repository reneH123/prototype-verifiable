// author: René Haberland, 19th July 2016
//
// IMPORTANT NOTICE:
//  purely experimental, use on own risk only, no guarantee whatsoever
//  may not be used/modified without the author's knowledge

grammar IRProlog;

options 
{
 language=Java;
}

@header
{
 import java.util.*;
}
@parser::members
{
}

prog:
 class_def EOF
| LSB ( localfunc ( COMMA localfunc )* | ) RSB EOF ;

localfunc: local_vars | func ;

class_def:
 CLASS LPAREN ID COMMA LSB fms RSB RPAREN ;

fms:
 fm ( COMMA fm )* | ;

fm: func | local_vars ;

func: FUNCTION LPAREN ID COMMA type COMMA params COMMA fun_body RPAREN ;

local_vars: VARIABLE LPAREN ID COMMA type ( COMMA expr | ) RPAREN ;

stm:
  FUNCTION
| ASSIGN LPAREN ID COMMA expr RPAREN
| local_vars
| RETURN 
| RETURN LPAREN expr RPAREN
| ALLOC LPAREN loc_1 RPAREN
| DELETE LPAREN loc_1 RPAREN
| FUNCALL LPAREN ID RPAREN
| FUNCALL LPAREN ID COMMA act_params RPAREN
| ITE LPAREN cond COMMA block RPAREN
| ITE LPAREN cond COMMA block COMMA block RPAREN
| WHILE LPAREN cond COMMA block RPAREN ;

block: stm
| LSB stm ( COMMA stm )* RSB ;

cond: ( AND | OR | rel ) LPAREN expr COMMA expr RPAREN
| NE LPAREN expr RPAREN
| TRUE | FALSE ;

rel: LT | LE | EQ | NE | GT | GE ;

act_params: expr 
| LSB expr ( COMMA expr )+ RSB ;

loc_1: ID 
| OA LPAREN ID COMMA ID RPAREN ;

loc: OFFSET LPAREN loc_1 ( COMMA offset | ) RPAREN
| loc_1 ;

offset: INT
| MINUS LPAREN INT COMMA INT RPAREN ;   // TODO: INT must be ZERO!

type: VOID | T_INT ;
 
params: LSB ( param ( COMMA param )* | ) RSB ;

param: PARAM LPAREN ID COMMA type RPAREN ;

fun_body: LSB ( stm ( COMMA stm )* | ) RSB ;

expr:
 ( ADD | SUB | MUL ) LPAREN expr COMMA expr RPAREN
| MEM LPAREN loc RPAREN
| loc
| INT
| FUNCALL LPAREN ID RPAREN
| FUNCALL LPAREN ID COMMA act_params RPAREN
;

////////////////////////////////////////////////////////

OA: 'oa';

CLASS: 'class';
VARIABLE: 'variable';

ASSIGN: 'assign';
RETURN: 'return';
ALLOC: 'alloc';
DELETE: 'delete';
ITE: 'ite';
WHILE: 'while';

AND: 'and';
OR: 'or';

ADD: 'add';
SUB: 'sub';
MUL: 'mul';
MEM: 'mem';
PARAM: 'param';
VOID: 'void';
T_INT: 'int';
MINUS: 'minus';
OFFSET: 'offset';

LPAREN: '(';
RPAREN: ')';
LSB: '[';
RSB: ']';
COMMA: ',';
DOT: '.';
FUNCALL: 'funcall';
FUNCTION: 'function';

FALSE: 'false';
TRUE: 'true';

LT: 'lt';
LE: 'le';
EQ: 'eq';
NE: 'ne';
GT: 'gt';
GE: 'ge';

////////////////////////////////////////////////////////

COMMENT: '//' (.)*? '\n' -> skip;
MULTILINECOMMENT: '/*' (.)*? '*/' -> skip;

ID  :   ('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')* ;
INT :   '0'..'9'+ ;
NEWLINE:'\r'? '\n' -> skip;
WS  :   (' '|'\t'|'\n'|'\r')+ -> skip;
