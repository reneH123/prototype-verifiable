% author: René Haberland, 19th July 2016

% IMPORTANT NOTICE:
%  purely experimental, use on own risk only, no guarantee whatsoever
%  may not be used/modified without the author's knowledge


statements:

void f(int a, int b)

{
  a=7;
   c=d;
}

<=> function(f,void,[param(a,int),param(b,int)],[assign(a,7),assign(c,d)])
	
void foo(int d);

<=> function(foo,void,[param(d,int)],[])

int main()
{
 int i=1;
  b=7;
}

<=> function(main,int,[],[var(i,int,1),assign(b,7)])


---------------------- IR-grammar

PROG ::= CLASS | ( LOCALS | FUNCTION )* 

CLASS ::= 'class' '(' ID ',' '[' FM ( ',' FM )* | eps ']' ')'

FM ::= FUNCTION | LOCALS  # class field or method

FUNCTION ::= 'function' '(' ID ',' TYPE ',' PARAMS ',' FUN_BODY ')'

LOCALS ::= 'variable' '(' ID ',' TYPE [ ',' EXPR ] ')'     # (uninitialised) local stack variables 

STM::=
  FUNCTION
| 'assign' '(' ID ',' EXPR ')'
| LOCALS
| 'return' [ '(' EXPR ')' ]
| 'new' '(' LOC_1 ')'
| 'delete' '(' LOC_1 ')'
| 'funcall' '(' ID [ ',' ACT_PARAMS ] ')'
| 'ite' '(' COND ',' BLOCK [ ',' BLOCK ] ')'
| 'while' '(' COND ',' BLOCK ')'

BLOCK ::= STM
| '[' STM ( ',' STM )* ']'

COND ::= ( 'and' | 'or' | REL ) '(' EXPR ',' EXPR ')'
| 'ne' '(' EXPR ')'
| 'true'
| 'false'

REL ::= 'lt' | 'le' | 'eq' | 'ne' | 'gt' | 'ge'

ACT_PARAMS ::= EXPR 
| '[' EXPR ( ',' EXPR )+ ']'

LOC_1 ::= ID 
| 'oa' '(' ID '.' ID ')'   # object accessor

LOC ::= 'offset' '(' LOC_1 [ OFFSET ] ')'

OFFSET ::= INT
| 'minus' '(' '0' ',' INT ')'

TYPE ::= 'void' | 'int'
 
PARAMS ::= '[' PARAM (',' PARAM)* | eps ']'
 (for example:  [param(a,int),param(b,int)])

PARAM ::= 'param' '(' ID ',' TYPE ')'

FUN_BODY ::= '[' STM (',' STM)* | eps ']'
 (for example: [assign(a,7),assign(c,d)])

EXPR ::=
 ( 'add' | 'sub' | 'mul' ) '(' EXPR ',' EXPR ')'
| 'mem' '(' LOC ')'
| LOC
| INT
| 'funcall' '(' ID [ ',' ACT_PARAMS ] ')'
