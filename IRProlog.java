// author: René Haberland, 19th July 2016
//
// IMPORTANT NOTICE:
//  purely experimental, use on own risk only, no guarantee whatsoever
//  may not be used/modified without the author's knowledge

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

public class IRProlog {

	public static void main(String[] args) throws Exception {
		ANTLRInputStream input = new ANTLRInputStream(System.in);
		IRPrologLexer lexer = new IRPrologLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		IRPrologParser parser = new IRPrologParser(tokens);

		/*ParseTree tree =*/ parser.prog();
		//System.out.println(tree.toStringTree(parser));
	}

}
