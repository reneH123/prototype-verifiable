#!/bin/sh

# author: René Haberland, 19th July 2016

# IMPORTANT NOTICE:
#  purely experimental, use on own risk only, no guarantee whatsoever
#  may not be used/modified without the author's knowledge


# clean up old Java binaries
rm *.class *~

# build parser and lexer from given G4 grammar
export CLASSPATH=".:/usr/local/lib/antlr-4.0-complete.jar:$CLASSPATH"
alias antlr4='java -jar /usr/local/lib/antlr-4.0-complete.jar'

antlr4 ClassBased.g4
antlr4 IRProlog.g4

# compile generated Java sources, result will be TestClassBased.class
javac *.java

# quick preparation of files needed to develop
gedit README.txt ClassBased.g4 IRProlog.g4 IR_Prolog.txt build.sh test.sh compile.sh astbuild.pl tmp.pl &
