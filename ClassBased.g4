// author: René Haberland, 19th July 2016

// IMPORTANT NOTICE:
//  purely experimental, use on own risk only, no guarantee whatsoever
//  may not be used/modified without the author's knowledge

//TODO: identfiers may not start with capital letter, or shall be lower-cased instead
//TODO: type checking; define scopes: (location, type), forward function_definitions
//TODO: declared variables being used only
//TODO: define + test assertion language
//TODO: generate/integrate SMT
//TODO: fast error recovery/fast syntax fails
grammar ClassBased;
options 
{
 language=Java;
}

@header
{
 import java.util.*;
}
@parser::members
{
String _tmp;
//TODO: bug: replace _list, _blist etc. by stack with LinkedList<String> objects!
LinkedList<String> _list = new LinkedList<String>();
LinkedList<String> _blist = new LinkedList<String>();
LinkedList<String> _aplist = new LinkedList<String>();
LinkedList<String> _varlist = new LinkedList<String>();
LinkedList<String> _fplist = new LinkedList<String>();
LinkedList<String> _fldmethlist = new LinkedList<String>();
LinkedList<String> _funlist = new LinkedList<String>();
// TODO: replace lists by a java.util.Stack!
// dump intermediate representation
static void dumpIR(String message)
{
  System.out.println(message);
}

// construct a list with comma-separated entries from 'as'. If 'as' is a singleton do not guard as list.
static String ConsList(LinkedList<String> as)
{
  int length = as.size();
  if(length==0)
    return "[]";
  else if(length==1)
    return as.getFirst();
  else
  {
    ListIterator<String> listIterator = as.listIterator();
    String _tmp = "[";
    _tmp+= listIterator.next();
    while(listIterator.hasNext())
    {
      _tmp+= ",";
      _tmp+= listIterator.next();
    }
    _tmp+= "]";
    return _tmp;
  }
} // ConsList

static String ConsVarsDelcarations(LinkedList<String> _varlist, String type, String expression)
{
  int length = _varlist.size();
  String _tmp;
  if(length==0)
    return "";
  else if(length==1)
  {
    _tmp = "variable("+_varlist.getFirst()+","+type;
    if(expression!="")
    {
      _tmp+=","+expression;
    }
    _tmp+=")";
    return _tmp;
  }
  else
  {
    ListIterator<String> listIterator = _varlist.listIterator();
    _tmp = "";
    _tmp+= "variable("+listIterator.next()+","+type;
    if(expression!="")
    {
      _tmp+=","+expression;
    }
    _tmp+=")";
    while(listIterator.hasNext())
    {
      _tmp+= ",";
      _tmp+= "variable("+listIterator.next()+","+type;
      if(expression!="")
      {
        _tmp+=","+expression;
      }
      _tmp+=")";
    }
    return _tmp;
  }
} // ConsVarsDelcarations

}

// TODO: only 1 class per program!
prog:
  // TODO: check 1 main function at the end
  // TODO: check forward/backward function definitions
  cd=class_definition SEMICOLON EOF {dumpIR($cd.IRName);}
 | {_funlist.clear();} (vd=variable_declaration SEMICOLON {_funlist.add($vd.IRName);} | fd=function_definition {_funlist.add($fd.IRName);} | fdcl=function_declaration SEMICOLON {_funlist.add($fdcl.IRName+",[])");} )* EOF {dumpIR(_funlist.toString());}
;

statement returns [String IRName]:
 assignment SEMICOLON {$IRName=$assignment.IRName;}
 | ite {$IRName=$ite.IRName;}
 | loop=whileloop {$IRName=$loop.IRName;}
 | allocate SEMICOLON {$IRName=$allocate.IRName;}
 | dispose SEMICOLON {$IRName=$dispose.IRName;}
 | funcall SEMICOLON {$IRName=$funcall.IRName;}
 | v=variable_declaration SEMICOLON {$IRName=$v.IRName;} // local stack variables
 | f=function_definition {$IRName=$f.IRName;}
 | RETURN {String _tmp="";} (e=expression {_tmp=$e.IRName;})? SEMICOLON 
   {
     if(_tmp=="") $IRName="return";
     else $IRName="return("+_tmp+")";
   }
;

// class(ID,modifier,[field,...],[method,...])
// TODO: mangle name with modifiers!
class_definition returns [String IRName]:
 class_modifier? CLASS id=ID LCPAREN cf=class_fields_methods RCPAREN
 {
   $IRName="class("+$id.text + "," + $cf.IRName + ")";
 }
;

class_fields_methods returns [String IRName]:
 {_fldmethlist.clear();} ( field=class_field {_fldmethlist.add($field.IRName);} | method=class_method {_fldmethlist.add($method.IRName);} )* 
  {$IRName=_fldmethlist.toString();}
;

class_field returns [String IRName]: 
 class_modifier? vd=variable_declaration SEMICOLON
 {$IRName=$vd.IRName;}
;

class_method returns [String IRName]: 
 class_modifier? fd=function_definition {$IRName=$fd.IRName;}
 | class_modifier? fdcl=function_declaration SEMICOLON {$IRName=$fdcl.IRName + ",[])";}
;

class_modifier: (PRIVATE | PUBLIC | PROTECTED) COLON
// TODO: encode modifier!
;

variable_declaration returns [String IRName]:
 type {_varlist.clear();} id1=ID {_varlist.add($id1.text);} (COMMA id2=ID {_varlist.add($id2.text);})*  {$IRName=ConsVarsDelcarations	(_varlist, $type.text, "");}
 | type {_varlist.clear();} id1=ID {_varlist.add($id1.text);} (COMMA id2=ID {_varlist.add($id2.text);})* EQUALS e1=expression {$IRName=ConsVarsDelcarations(_varlist, $type.text, $e1.IRName);}
;

function_definition returns [String IRName]:
 fhead=function_declaration block {$IRName=$fhead.IRName+","+$block.IRName+")";}
 // TODO: only function definitions may have a parameter type list
 // TODO: if function has a return value, function body has to return a value
;

// fun_decl(ID,type,params)
function_declaration returns [String IRName]:
 t=type_or_void fname=ID {String _tmp2="function("+$fname.text+","+$t.text;} {_tmp2+=",[]";} LPAREN RPAREN {$IRName=_tmp2;}
 | t=type_or_void fname=ID {String _tmp2="function("+$fname.text+","+$t.text;} LPAREN (params=formal_parameters {_tmp2+=","+$params.IRName;}) RPAREN {$IRName=_tmp2;}
;

type:
 INTEGER
;

type_or_void:
 type
 | VOID
;

expression returns [String IRName]:
 e1=expression_3 PLUS e2=expression_3 {$IRName="add("+$e1.IRName+","+$e2.IRName+")";}
 | e1=expression_3 MINUS e2=expression_3 {$IRName="sub("+$e1.IRName+","+$e2.IRName+")";}
 | e1=expression_3 {$IRName=$e1.IRName;}
;

expression_3 returns [String IRName]:
 e1=expression_2 MULT e2=expression_2 {$IRName="mul("+$e1.IRName+","+$e2.IRName+")";}
 | e1=expression_2 {$IRName=$e1.IRName;}
;

expression_2 returns [String IRName]:
 MINUS e=expression_1 {$IRName="sub(0,"+$e.IRName+")";}
 | e1=expression_1 {$IRName=$e1.IRName;}
;

expression_1 returns [String IRName]:
 LSBRACKET loc=location RSBRACKET {$IRName="mem("+$loc.IRName+")";}
 | LPAREN e=expression RPAREN {$IRName=$e.IRName;}
 | funcall {$IRName=$funcall.IRName;}
 | loc=location {$IRName=$loc.IRName;}
 | INT {$IRName=$INT.text;}
;

location_1 returns [String IRName]:
 ID {$IRName=$ID.text;}
 | object=ID DOT field=ID {$IRName="oa("+$object.text+","+$field.text+")";} // object accessor 
 | THIS DOT ID {$IRName=$ID.text;}
;

location returns [String IRName]:
  loc=location_1 offset {$IRName="offset("+$loc.IRName+","+$offset.IRName+")";}
 | loc=location_1 {$IRName=$loc.IRName;} ;

offset returns [String IRName]:
 PLUS INT {$IRName=$INT.text;}
 | MINUS INT {$IRName="minus(0,"+$INT.text+")";}
;

assignment returns [String IRName]:
  a=assignment_LHS EQUALS {_list.clear();} (as=assignment_LHS EQUALS {_list.add($as.IRName);})* e=expression 
{
  String _tmp;

  if(_list.size()>0)
  {
    _tmp = "[";
    _tmp+="assign(" + $a.IRName + "," + $e.IRName + ")";
    ListIterator<String> listIterator = _list.listIterator();
    while (listIterator.hasNext())
    {
     _tmp+=",";
     _tmp+="assign(" + listIterator.next() + "," + $e.IRName + ")";
    }
    _tmp+= "]";
  }
  else
  {
    _tmp="assign(" + $a.IRName + "," + $e.IRName + ")";
  }
  $IRName = _tmp;
}
;

assignment_LHS returns [String IRName]:
 location_1 {$IRName=$location_1.IRName;}
 | LSBRACKET loc=location RSBRACKET {$IRName="mem("+$loc.IRName+")";}
;

block returns [String IRName]:
 LCPAREN {_blist.clear();} (stmt=statement {_blist.add($stmt.IRName);})* RCPAREN {$IRName=ConsList(_blist);}
;

ite returns [String IRName]:
 IF LPAREN condition RPAREN b1=blockorstatement {$IRName="ite("+$condition.IRName+","+$b1.IRName+")";}
 | IF LPAREN condition RPAREN b1=blockorstatement ( ELSE b2=blockorstatement )? {$IRName="ite("+$condition.IRName+","+$b1.IRName+","+$b2.IRName+")";}
;

blockorstatement returns [String IRName]:
 block {$IRName=$block.IRName;}
 | statement {$IRName="[" + $statement.IRName + "]";}
;

whileloop returns [String IRName]:
 WHILE LPAREN condition RPAREN b1=blockorstatement {$IRName="while("+$condition.IRName+","+$b1.IRName+")";}
;

allocate returns [String IRName]:
 NEW LPAREN loc=location_1 RPAREN {$IRName="alloc("+$loc.IRName+")";}
;

dispose returns [String IRName]:
 DELETE LPAREN loc=location_1 RPAREN {$IRName="delete("+$loc.IRName+")";}
;

funcall returns [String IRName]:
 (THIS DOT)? id=ID LPAREN actuals=actual_parameters RPAREN {$IRName="funcall("+$id.text+","+$actuals.IRName+")";}
 | (THIS DOT)? id=ID LPAREN RPAREN {$IRName="funcall("+$id.text+")";}
 // TODO: actual_parameters combined with and validated (e.g. type checked) against formal_parameters (looked-up in temporary function environment)!
;

actual_parameters returns [String IRName]:
 {_aplist.clear();} e1=expression {_aplist.add($e1.IRName);} ( COMMA e2=expression {_aplist.add($e2.IRName);} ) * {$IRName=ConsList(_aplist);} 
;

formal_parameters returns [String IRName]:
 t1=type id1=ID {String _tmp2="";} ( COMMA t2=type id2=ID {_tmp2+=",param("+$id2.text+","+$t2.text+")";} )* 
 {
   _tmp2 = "param("+$id1.text+","+$t1.text+")" + _tmp2;
   if(_tmp2!=""){
     _tmp2 = "[" + _tmp2 + "]";
   }
   $IRName = _tmp2;
 }
;

condition returns [String IRName]:
 e1=expression op=(AND|OR) e2=expression {$IRName=$op.text+"("+$e1.IRName+","+$e2.IRName+")";}
 | e1=expression rel=relation e2=expression {$IRName=$rel.IRName+"("+$e1.IRName+","+$e2.IRName+")";}
 | NEGATE e=expression {$IRName="ne("+$e.IRName+")";}
 | e=expression {$IRName=$e.IRName;}
 | TRUE {$IRName="true";}
 | FALSE {$IRName="false";}
;

relation returns [String IRName]:
 LT {$IRName="lt";}
 | LE {$IRName="le";}
 | EQ {$IRName="eq";}
 | NE {$IRName="ne";}
 | GT {$IRName="gt";}
 | GE {$IRName="ge";}
;

// assertion language

assertion:
 LSBRACKET assertion_expression RSBRACKET ;	

assertion_2:
 TRUE | FALSE 
 | assertion_expression ( EQ | NE ) assertion_expression
 | assertion_2 AND assertion_2 | NEGATE assertion_2 ; 

assertion_expression:
 ID | INT
 | assertion_expression ( PLUS | MINUS | MULT ) assertion_expression
 ;

/*
Simple programming language: "vernacular"

S::= x:=E | while B do S | if B then S else S | S;S
B::= E <= E | E>=E | !B
E::= x|i|E (+|-|*) E


Assertion language:  "proof language"

Spec::= true|false|E (==|!=) E | Spec /\ Spec | !Spec
E::= x|i|E(+|-|*|^) E


Reynold's language:
Cmd::= var = new exp | var = [exp] | [exp] = exp | dispose exp.

H::= emp| x|->y0,...,yk| H * H | H -* H | ... missing: functions!
*/


/*
// class based:
a,b::=x
 |new C()
 |a.f
 |a.f=b
 |a.m
 |let x=a in b
c::=class C{f_i, m_i, C()=b}

// object based:
a,b::=x|a.f|a.f=b|a.m|a.m=\psi(x)b| [f_i=b_i, m_j=\psi(x_i)b_j] | clone(a) | let x=a in b

a,b::=x
 |false|true
 |if x then a0 else a1
 |let x=a in b
 |[f_i=x_i], m_j=\psi(y_j)b_j]
 |x.f
 |x.f:=y
 |x.m
*/



/*
AST syntax definition:
TODO: ...
*/
EQUALS: '=';
WHILE: 'while';
LPAREN: '(';
RPAREN: ')';
LCPAREN: '{';
RCPAREN: '}';	
LSBRACKET: '[';
RSBRACKET: ']';
SEMICOLON: ';';
COLON: ':';
LT: '<';
LE: '<=';
EQ: '==';
NE: '!=';
GT: '>';
GE: '>=';
TRUE: 'true';
FALSE: 'false';
NEGATE: '!';
PLUS: '+';
MINUS: '-';
MULT: '*';
IF: 'if';
ELSE: 'else';
AND: '&&';
OR: '||';
NEW: 'new';
DELETE: 'delete';
DOT: '.';
COMMA: ',';
INTEGER: 'int';
VOID: 'void';
RETURN: 'return';
CLASS: 'class';
PRIVATE: 'private';
PROTECTED: 'protected';
PUBLIC: 'public';
THIS: 'this';

COMMENT: '//' (.)*? '\n' -> skip;
MULTILINECOMMENT: '/*' (.)*? '*/' -> skip;

ID  :   ('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')* ;
INT :   '0'..'9'+ ;
NEWLINE:'\r'? '\n' -> skip;
WS  :   (' '|'\t'|'\n'|'\r')+ -> skip;
